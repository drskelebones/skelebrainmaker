# Skelebrainmaker

Ever wanted to make a Markov chain model off a Twitter account and save it in a parsable JSON file?
Now you can!

This was intended for use with [Skelebot](https://gitlab.com/drskelebones/skelebot) future Markov functionality.

![billnye](https://cdn.skele.dev/rigid-grim-silver-big-apatosaur/direct)

## Resources

- [gomarkov](https://github.com/mb-14/gomarkov)
- [twitter-scraper](https://github.com/n0madic/twitter-scraper)

## Build

`make`

## Run

`./skelebrainmaker --user [twitter handle] --sample [sample size]`

`--user` defaults to `@Twitter`

`--sample` defaults to 100
