package main

import (
	"bufio"
	"context"
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"strings"
	"sync"

	. "github.com/logrusorgru/aurora"
	"github.com/mb-14/gomarkov"
	twitter "github.com/n0madic/twitter-scraper"
)

var (
	user        string
	sample_size int
	wg          sync.WaitGroup
)

func init() {
	flag.StringVar(&user, "user", "Twitter", "Twitter user")
	flag.IntVar(&sample_size, "sample", 100, "Sample size")
	flag.Parse()
}

func main() {
	chain := gomarkov.NewChain(1)
	scraper := twitter.New()
	fmt.Println(fmt.Sprintf("%s @%s", Magenta("Training on"), user))
	for tweet := range scraper.GetTweets(context.Background(), user, sample_size) {
		wg.Add(1)
		go Train(tweet.Text, chain)
	}
	wg.Wait()
	fmt.Println(fmt.Sprintf("%s%s", Magenta("Saving to "), user+".brain"))
	Save(user+".brain", chain)
	fmt.Println(fmt.Sprintf("%s", Green("Samples: ")))
	for i := 0; i < 5; i++ {
		fmt.Println("- " + GenerateResponse(chain))
	}
}

func Train(tweet string, c *gomarkov.Chain) {
	defer wg.Done()
	scanner := bufio.NewScanner(strings.NewReader(tweet))
	for scanner.Scan() {
		c.Add(strings.Split(scanner.Text(), " "))
	}
}

func Load(name string) (*gomarkov.Chain, error) {
	var chain gomarkov.Chain
	data, err := ioutil.ReadFile(name)
	if err != nil {
		return &chain, err
	}
	err = json.Unmarshal(data, &chain)
	if err != nil {
		return &chain, err
	}
	return &chain, nil
}

func Save(name string, c *gomarkov.Chain) {
	jsonObj, _ := json.Marshal(c)
	err := ioutil.WriteFile(name, jsonObj, 0644)
	if err != nil {
		fmt.Println(err)
	}
}

func GenerateResponse(chain *gomarkov.Chain) string {
	tokens := []string{gomarkov.StartToken}
	for tokens[len(tokens)-1] != gomarkov.EndToken {
		next, _ := chain.Generate(tokens[(len(tokens) - 1):])
		tokens = append(tokens, next)
	}
	return strings.Join(tokens[1:len(tokens)-1], " ")
}
