module skelebrainmaker

go 1.17

require (
	github.com/logrusorgru/aurora v2.0.3+incompatible
	github.com/mb-14/gomarkov v0.0.0-20210216094942-a5b484cc0243
	github.com/n0madic/twitter-scraper v0.0.0-20220111124717-750e06df2493
)

require golang.org/x/net v0.0.0-20220121210141-e204ce36a2ba // indirect
